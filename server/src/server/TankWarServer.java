package server;

import server.component.Lobby;
import server.component.LoginManager;

public class TankWarServer {

	private static TankWarServer instance = new TankWarServer();
	private LoginManager loginManager;
	private Lobby lobby;
	
	private TankWarServer() {
        setLoginManager(new LoginManager());
        setLobby(new Lobby());
    }

    public static TankWarServer getInstance() {
    	return instance;
    }

	public void start() {
		getLoginManager().start();
	}
	
	public LoginManager getLoginManager() {
		return loginManager;
	}

	private void setLoginManager(LoginManager loginManager) {
		this.loginManager = loginManager;
	}

	public Lobby getLobby() {
		return this.lobby;
	}
	
	public void setLobby(Lobby lobby) {
		this.lobby = lobby;
	}
	
}
