package server.component;

public class CommThread {

	private String id;
	private PassiveThread passiveThread;
	private ActiveThread activeThread;
	
	public CommThread() {
		
	}
	
	public String getId() {
		return this.id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public PassiveThread getPassiveThread() {
		return passiveThread;
	}

	public void setPassiveThread(PassiveThread passiveThread) {
		this.passiveThread = passiveThread;
	}

	public ActiveThread getActiveThread() {
		return activeThread;
	}

	public void setActiveThread(ActiveThread activeThread) {
		this.activeThread = activeThread;
	}
	
}
