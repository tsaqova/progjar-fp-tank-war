package server.component;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import common.config.Config;

import server.abstraction.AServer;

public class ActiveServer extends AServer {

	public ActiveServer(CommServer commServer) {
		super(commServer);
	}

	@Override
	public void run() {
		try {
			setServerSocket(new ServerSocket(Config.PORT_ACTIVE));
			while(true) {
				Socket socket = getServerSocket().accept();
				ActiveThread activeThread = new ActiveThread(socket);
				activeThread.start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
