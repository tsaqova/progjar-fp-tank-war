package server.component;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import common.config.Config;

import server.abstraction.AServer;

public class PassiveServer extends AServer {

	public PassiveServer(CommServer commServer) {
		super(commServer);
	}
	
	@Override
	public void run() {
		try {
			setServerSocket(new ServerSocket(Config.PORT_PASSIVE));
			while(true) {
				Socket socket = getServerSocket().accept();
				PassiveThread passiveThread = new PassiveThread(this, socket);
				passiveThread.start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}

}
