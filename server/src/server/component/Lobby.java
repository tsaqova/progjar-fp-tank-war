package server.component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import server.TankWarServer;
import common.component.Chat;
import common.component.Player;
import common.component.Room;

public class Lobby {

	private HashMap<String, Room> roomMap;
	private List<String> listUser;
	
	public Lobby() {
		setRoomMap(new HashMap<String, Room>());
		setListUser(new ArrayList<String>());
	}
	
	public Boolean addRoom(Room room) throws IOException {
		if(!getRoomMap().containsKey(room.getName())) { 
			getRoomMap().put(room.getName(), room);
			return true;
		}
		return false;
	}
	
	public Room getRoom(String name) {
		if(getRoomMap().containsKey(name)) {
			return getRoomMap().get(name);
		}
		return null;
	}
	
	public Boolean removeRoom(String name) throws IOException {
		if(getRoomMap().containsKey(name)) {
			getRoomMap().remove(name);
			return true;
		}
		return false;
	}
	
	public Boolean addUser(String name) throws IOException {
		if(!getListUser().contains(name)) { 
			getListUser().add(name);
			return true;
		}
		return false;
	}
	
	public Boolean removeUser(String name) throws IOException {
		if(getListUser().contains(name)) {
			getListUser().remove(name);
			return true;
		}
		return false;
	}
	
	private HashMap<String, Room> getRoomMap() {
		return roomMap;
	}

	private void setRoomMap(HashMap<String, Room> roomMap) {
		this.roomMap = roomMap;
	}

	public List<String> getListUser() {
		return listUser;
	}

	public void setListUser(List<String> listUser) {
		this.listUser = listUser;
	}
	
	public void broadCastRooms() throws IOException {
		List<Room> listRooms = getListRooms();
		for(String user : listUser) {
			CommThread commThread = TankWarServer.getInstance().getLoginManager().getCommThread(user);
			commThread.getPassiveThread().sendListRoom(listRooms);
		}
	}
	
	public List<Room> getListRooms() {
		List<Room> listRooms = new ArrayList<Room>();
		for (Map.Entry<String, Room>entry : getRoomMap().entrySet()) {
			listRooms.add(entry.getValue());
		}
		return listRooms;
	}
	
	public void broadCastChat(Chat chat) throws IOException {
		TankWarServer server = TankWarServer.getInstance();
		List<String> listUsers = getListUser();
		for(String user : listUsers) {
			CommThread commthread = server.getLoginManager().getCommThread(user);
			commthread.getPassiveThread().sendChat(chat);
		}
	}
	
	public void broadCastListUser() throws IOException {
		List<String> listUsers = getListUser();
		List<Player> listPlayers = new ArrayList<Player>();
		for(String user : listUsers) {
			listPlayers.add(new Player(user));
		}
		for(String user : listUsers) {
			CommThread commThread = TankWarServer.getInstance().getLoginManager().getCommThread(user);
			commThread.getPassiveThread().sendListPlayers(listPlayers);
		}
	}
	
}
