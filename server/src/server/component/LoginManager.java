package server.component;

import java.util.HashMap;

public class LoginManager {
	
	private CommServer commServer;
	private HashMap<String, CommThread> commMap;

	public LoginManager() {
		setCommServer(new CommServer());
		setCommMap(new HashMap<String, CommThread>());
	}
	
	public void start() {
		getCommServer().start();
	}
	
	public Boolean addCommThread(CommThread commThread) {
		if(!getCommMap().containsKey(commThread.getId())) {
			getCommMap().put(commThread.getId(), commThread);
			return true;
		}
		return false;
	}
	
	public CommThread getCommThread(String id) {
		if(getCommMap().containsKey(id)) {
			return getCommMap().get(id);
		}
		return null;
	}
	
	public Boolean removeCommThread(String id) {
		if(getCommMap().containsKey(id)) {
			getCommMap().remove(id);
			return true;
		}
		return false;
	}
	
	public Boolean removeCommThread(CommThread commThread) {
		if(getCommMap().containsValue(commThread)) {
			getCommMap().remove(commThread.getId());
			return true;
		}
		return false;
	}
	
	private CommServer getCommServer() {
		return commServer;
	}

	private void setCommServer(CommServer commServer) {
		this.commServer = commServer;
	}
	
	private HashMap<String, CommThread> getCommMap() {
		return commMap;
	}

	private void setCommMap(HashMap<String, CommThread> commMap) {
		this.commMap = commMap;
	}
	
}
