package server.component;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import common.abstraction.AObject;
import common.clientcomm.ClientCommLogin;
import common.component.Chat;
import common.component.Player;
import common.component.Room;
import common.config.CommMessage;
import common.servercomm.ServerCommLogin;
import common.servercomm.ServerCommRefreshRoom;
import common.servercomm.ServerCommRefreshTeam;
import common.servercomm.ServerCommRefreshUser;
import common.servercomm.ServerCommSendChat;
import common.servercomm.ServerCommSendListObject;
import common.servercomm.ServerCommStartGame;
import server.TankWarServer;
import server.abstraction.AServer;
import server.abstraction.AThread;
import server.common.Message;

public class PassiveThread extends AThread {

	public PassiveThread(AServer server, Socket socket)
			throws IOException {
		super(socket);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void run() {
		try {
			ClientCommLogin ccl = (ClientCommLogin) getOis().readObject();
			LoginManager loginManager = TankWarServer.getInstance().getLoginManager();
			CommThread commThread = loginManager.getCommThread(ccl.getUsername());
			commThread.setPassiveThread(this);
			setCommThread(commThread);
			ServerCommLogin scl = new ServerCommLogin();
			scl.setMsg(CommMessage.OK);
			getOos().writeObject(scl);
			getOos().flush();
			System.out.println(Message.ConnectedToServer(commThread.getId()));
			Lobby lobby = TankWarServer.getInstance().getLobby();
			lobby.addUser(ccl.getUsername());
			List<Room> listRooms = TankWarServer.getInstance().getLobby().getListRooms();
			ServerCommRefreshRoom scrr = new ServerCommRefreshRoom(listRooms);
			getOos().writeObject(scrr);
			getOos().flush();
			lobby.broadCastListUser();
			lobby.broadCastRooms();
			while(!getThread().isInterrupted()) {
				// wait interupt from activethread
			}
			lobby.broadCastListUser();
			lobby.broadCastRooms();
			closeSocket();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void sendListRoom(List<Room> listRooms) throws IOException {
		ServerCommRefreshRoom scrr = new ServerCommRefreshRoom(listRooms);
		getOos().reset();
		getOos().writeObject(scrr);
		getOos().flush();
	}
	
	public void sendListTeam(List<Player> listPlayersA, List<Player> listPlayersB) throws IOException {
		ServerCommRefreshTeam scrt = new ServerCommRefreshTeam(listPlayersA, listPlayersB);
		System.out.println(listPlayersA.size());
		System.out.println(listPlayersB.size());
		getOos().reset();
		getOos().writeObject(scrt);
		getOos().flush();
	}
	
	public void sendChat(Chat chat) throws IOException {
		ServerCommSendChat scsc = new ServerCommSendChat(chat);
		getOos().reset();
		getOos().writeObject(scsc);
		getOos().flush();
	}

	public void sendListPlayers(List<Player> listPlayers) throws IOException {
		ServerCommRefreshUser scru = new ServerCommRefreshUser(listPlayers);
		getOos().reset();
		getOos().writeObject(scru);
		getOos().flush();
	}

	public void sendStartGame() throws IOException {
		ServerCommStartGame scsg = new ServerCommStartGame();
		getOos().reset();
		getOos().writeObject(scsg);
		getOos().flush();
	}

	public void sendListObject(List<AObject> list) throws IOException {
		ServerCommSendListObject scslo = new ServerCommSendListObject(list);
		getOos().reset();
		getOos().writeObject(scslo);
		getOos().flush();
	}

}
