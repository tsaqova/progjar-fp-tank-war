package server.component;

public class CommServer {

	private ActiveServer activeServer;
	private PassiveServer passiveServer;
	
	public CommServer() {
		setActiveServer(new ActiveServer(this));
		setPassiveServer(new PassiveServer(this));
	}
	
	private ActiveServer getActiveServer() {
		return activeServer;
	}
	
	private void setActiveServer(ActiveServer activeServer) {
		this.activeServer = activeServer;
	}
	
	private PassiveServer getPassiveServer() {
		return passiveServer;
	}
	
	private void setPassiveServer(PassiveServer passiveServer) {
		this.passiveServer = passiveServer;
	}
	
	public void start() {
		getPassiveServer().start();
		getActiveServer().start();
	}
	
}
