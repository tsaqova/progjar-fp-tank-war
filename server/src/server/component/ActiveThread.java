package server.component;

import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.util.List;

import com.sun.security.ntlm.Client;

import common.abstraction.AComm;
import common.clientcomm.ClientCommLeaveRoom;
import common.clientcomm.ClientCommChangeTeam;
import common.clientcomm.ClientCommCreateRoom;
import common.clientcomm.ClientCommJoinRoom;
import common.clientcomm.ClientCommLogin;
import common.clientcomm.ClientCommLogout;
import common.clientcomm.ClientCommMove;
import common.clientcomm.ClientCommSendChat;
import common.clientcomm.ClientCommShoot;
import common.clientcomm.ClientCommStartGame;
import common.component.Chat;
import common.component.Player;
import common.component.Room;
import common.component.Status;
import common.config.CommMessage;
import common.servercomm.ServerCommChangeTeam;
import common.servercomm.ServerCommCreateRoom;
import common.servercomm.ServerCommJoinRoom;
import common.servercomm.ServerCommLeaveRoom;
import common.servercomm.ServerCommLogin;
import common.servercomm.ServerCommLogout;
import common.servercomm.ServerCommRefreshRoom;
import common.servercomm.ServerCommSendChat;
import server.TankWarServer;
import server.abstraction.AThread;
import server.common.Message;

public class ActiveThread extends AThread {

	public ActiveThread(Socket socket)
			throws IOException {
		super(socket);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void run() {
		AComm comm;
		try {
			while((comm = (AComm) getOis().readObject()) != null) {
				if(comm instanceof ClientCommLogin) {
					login((ClientCommLogin)comm);
				}
				else if(comm instanceof ClientCommCreateRoom) {
					createRoom((ClientCommCreateRoom)comm);
				}
				else if(comm instanceof ClientCommJoinRoom) {
					joinRoom((ClientCommJoinRoom) comm);
				}
				else if(comm instanceof ClientCommChangeTeam) {
					changeTeam((ClientCommChangeTeam)comm);
				}
				else if(comm instanceof ClientCommLogout) {
					Lobby lobby = TankWarServer.getInstance().getLobby();
					if(((ClientCommLogout) comm).getStatus().equals(Status.LOBBY)) {
						lobby.removeUser(((ClientCommLogout) comm).getUserName());
					}
					else if(((ClientCommLogout) comm).getStatus().equals(Status.TEAMSELECT)) {
						Room room = lobby.getRoom(((ClientCommLogout) comm).getRoomName());
						room.removePlayer(((ClientCommLogout) comm).getUserName());
					}
					ServerCommLogout scl = new ServerCommLogout();
					scl.setMsg(CommMessage.OK);
					getOos().writeObject(scl);
					getOos().flush();
					break;
				}
				else if(comm instanceof ClientCommSendChat) {
					Lobby lobby = TankWarServer.getInstance().getLobby();
					lobby.broadCastChat(((ClientCommSendChat) comm).getChat());
				}
				else if(comm instanceof ClientCommLeaveRoom) {
					System.out.println("Room : " + ((ClientCommLeaveRoom) comm).getRoomName());
					System.out.println("Player : " + ((ClientCommLeaveRoom) comm).getUserName());
					Room room = TankWarServer.getInstance().getLobby().getRoom(((ClientCommLeaveRoom) comm).getRoomName());
					ServerCommLeaveRoom sclr = new ServerCommLeaveRoom();
					if(room.moveToLobby(((ClientCommLeaveRoom) comm).getUserName())) {
						room.broadCastTeam();
						sclr.setMsg(CommMessage.OK);
					}
					else {
						sclr.setMsg(CommMessage.FAIL);
					}
					Lobby lobby = TankWarServer.getInstance().getLobby();
					lobby.broadCastListUser();
					lobby.broadCastRooms();
					
					getOos().writeObject(sclr);
					getOos().flush();
				}
				else if(comm instanceof ClientCommStartGame) {
					System.out.println("menerima perintah start game dr client");
					startGame((ClientCommStartGame)comm);
				}
				else if(comm instanceof ClientCommMove) {
					Room room = TankWarServer.getInstance().getLobby().getRoom(((ClientCommMove) comm).getRoomName());
					String username = ((ClientCommMove) comm).getUsername();
					int direction = ((ClientCommMove) comm).getDirection();
					room.moveObjects(username, direction);
				}
				else if(comm instanceof ClientCommShoot) {
					Room room = TankWarServer.getInstance().getLobby().getRoom(((ClientCommShoot) comm).getRoomName());
					String username = ((ClientCommShoot) comm).getUsername();
					int direction = ((ClientCommShoot) comm).getDirection();
					room.shootObjects(username, room, direction);
				}
			}
		}  catch (EOFException e) {
			// Go to finally
		}  catch (SocketException e) {
			// Go to finally
		}  catch (Exception e) {
			e.printStackTrace();
		}	finally {
			PassiveThread passiveThread = getCommThread().getPassiveThread();
			passiveThread.getThread().interrupt();
			LoginManager loginManager = TankWarServer.getInstance().getLoginManager();
			if(loginManager.removeCommThread(getCommThread().getId())) {
				System.out.println(Message.DisconnectedFromServer(getCommThread().getId()));
			}
			else {
				System.out.println("Gagal DC");
			}
			try {
				closeSocket();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void startGame(ClientCommStartGame comm) throws IOException {
		System.out.println("active thread jalanin room start");
		Room room = TankWarServer.getInstance().getLobby().getRoom(comm.getRoomName());
		room.start();
	}

	private void joinRoom(ClientCommJoinRoom comm) throws IOException {
		Lobby lobby = TankWarServer.getInstance().getLobby();
		ServerCommJoinRoom scjr;
		String roomName = lobby.getRoom(comm.getRoomName()).addPlayer(new Player(comm.getUserName())); 
		if(roomName.equals("Team A")) {
			scjr = new ServerCommJoinRoom("Team A");
			scjr.setMsg(CommMessage.OK);
			lobby.removeUser(comm.getUserName());
			lobby.broadCastListUser();
		}
		else if(roomName.equals("Team B")) {
			scjr = new ServerCommJoinRoom("Team B");
			scjr.setMsg(CommMessage.OK);
			lobby.removeUser(comm.getUserName());
			lobby.broadCastListUser();
		}
		else {
			scjr = new ServerCommJoinRoom("");
			scjr.setMsg(CommMessage.ROOMFULL);
		}
		getOos().writeObject(scjr);
		getOos().flush();
		lobby.getRoom(comm.getRoomName()).broadCastTeam();
	}

	private void createRoom(ClientCommCreateRoom comm) throws IOException {
		Room room = new Room(comm.getRoomName(), new Player(getCommThread().getId()));
		Lobby lobby = TankWarServer.getInstance().getLobby();
		ServerCommCreateRoom sccr = new ServerCommCreateRoom();
		if(lobby.addRoom(room)) {
			sccr.setMsg(CommMessage.OK);
			lobby.removeUser(getCommThread().getId());
			room.broadCastTeam();
			lobby.broadCastRooms();
			lobby.broadCastListUser();
		}
		else {
			sccr.setMsg(CommMessage.FAIL);
		}
		getOos().writeObject(sccr);
		getOos().flush();
	}

	private void login(ClientCommLogin ccl) throws IOException {
		CommThread commThread = new CommThread();
		commThread.setId(ccl.getUsername());
		commThread.setActiveThread(this);
		setCommThread(commThread);
		LoginManager loginManager = TankWarServer.getInstance().getLoginManager();
		ServerCommLogin serverCommLogin = new ServerCommLogin();
		if(loginManager.addCommThread(commThread)) {
			serverCommLogin.setMsg(CommMessage.OK);
		}
		else {
			serverCommLogin.setMsg(CommMessage.FAIL);
		}
		getOos().writeObject(serverCommLogin);
		getOos().flush();
	}
	
	private void changeTeam(ClientCommChangeTeam comm) throws IOException {
		Room room = TankWarServer.getInstance().getLobby().getRoom(comm.getRoomName());
		Player player = new Player(comm.getUserName());
		player.setRoomName(comm.getRoomName());
		player.setTeamName(comm.getFromTeam());
		ServerCommChangeTeam scct = new ServerCommChangeTeam();
		if(room.changeTeam(player, comm.getToTeam())) {
			scct.setMsg(CommMessage.OK);
		}
		else {
			scct.setMsg(CommMessage.FAIL);
		}
		getOos().writeObject(scct);
		getOos().flush();
	}

}
