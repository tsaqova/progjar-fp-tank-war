package server.abstraction;

import java.net.ServerSocket;
import server.component.CommServer;

public abstract class AServer implements Runnable {
	
	private CommServer commServer;
	private ServerSocket serverSocket;
    private Thread thread;
    
	private AServer() {
    	
    }
    
	protected AServer(CommServer commserver) {
		this();
    	setCommServer(commserver);
    }
	
    public void start() {
    	setThread(new Thread(this));
    	getThread().start();
    }
		
	public CommServer getCommSocketServer() {
		return commServer;
	}

	protected void setCommServer(CommServer commServer) {
		this.commServer = commServer;
	}

	protected ServerSocket getServerSocket() {
		return serverSocket;
	}

	protected void setServerSocket(ServerSocket serverSocket) {
		this.serverSocket = serverSocket;
	}
	
    private Thread getThread() {
		return thread;
	}

	private void setThread(Thread thread) {
		this.thread = thread;
	}
	
}
