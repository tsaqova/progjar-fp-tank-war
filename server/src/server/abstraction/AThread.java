package server.abstraction;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import server.component.CommThread;

public abstract class AThread implements Runnable {

	private Thread thread;
    private Socket socket;
    private CommThread commThread;
	private ObjectOutputStream oos;
	private ObjectInputStream ois;
	
    private AThread() {
    	
    }
    
    public AThread(Socket socket) throws IOException {
    	this();
		setSocket(socket);
		setOos(new ObjectOutputStream(getSocket().getOutputStream()));
		setOis(new ObjectInputStream(getSocket().getInputStream()));
    }
    
	public void start() {
    	if(thread == null) {
    		thread = new Thread(this);
    		thread.start();
    	}
	}
    
	public void closeSocket() throws IOException {
		getOos().close();
		getOis().close();
		getSocket().close();
	}
	
    public Thread getThread() {
		return thread;
	}
    
	public void setThread(Thread thread) {
		this.thread = thread;
	}
	
	public Socket getSocket() {
		return socket;
	}
	
	public void setSocket(Socket socket) {
		this.socket = socket;
	}
	
    public ObjectOutputStream getOos() {
		return oos;
	}

	public void setOos(ObjectOutputStream oos) {
		this.oos = oos;
	}

	public ObjectInputStream getOis() {
		return ois;
	}

	public void setOis(ObjectInputStream ois) {
		this.ois = ois;
	}

    public CommThread getCommThread() {
		return commThread;
	}

    public void setCommThread(CommThread commThread) {
		this.commThread = commThread;
	}
	
}
