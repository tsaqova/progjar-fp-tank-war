package common.clientcomm;

import common.abstraction.AComm;

public class ClientCommChangeTeam extends AComm {

	private String userName;
	private String roomName;
	private String fromTeam;
	private String toTeam;
	
	public ClientCommChangeTeam(String userName, String roomName, String fromTeam, String toTeam) {
		setUserName(userName);
		setRoomName(roomName);
		setFromTeam(fromTeam);
		setToTeam(toTeam);
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getFromTeam() {
		return fromTeam;
	}

	public void setFromTeam(String fromTeam) {
		this.fromTeam = fromTeam;
	}

	public String getToTeam() {
		return toTeam;
	}

	public void setToTeam(String toTeam) {
		this.toTeam = toTeam;
	}

}
