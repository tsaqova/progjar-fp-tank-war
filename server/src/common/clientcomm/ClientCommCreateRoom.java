package common.clientcomm;

import java.io.IOException;

import common.abstraction.AComm;
import common.component.Player;
import common.component.Room;
import common.config.CommMessage;

public class ClientCommCreateRoom extends AComm {

	String roomName;
	
	public ClientCommCreateRoom(String roomName) throws IOException {
		setMsg(CommMessage.CREATEROOM);
		setRoomName(roomName);
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}


}
