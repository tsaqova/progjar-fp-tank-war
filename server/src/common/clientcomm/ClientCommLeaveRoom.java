package common.clientcomm;

import common.abstraction.AComm;

public class ClientCommLeaveRoom extends AComm {

	private String userName;
	private String roomName;
	
	public ClientCommLeaveRoom(String userName, String roomName) {
		setUserName(userName);
		setRoomName(roomName);
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}
