package common.clientcomm;

import common.abstraction.AComm;
import common.component.Player;
import common.config.CommMessage;

public class ClientCommJoinRoom extends AComm {
	
	private String userName;
	private String roomName;
	
	public ClientCommJoinRoom(String userName, String roomName) {
		setMsg(CommMessage.JOINROOM);
		setUserName(userName);
		setRoomName(roomName);
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	
}
