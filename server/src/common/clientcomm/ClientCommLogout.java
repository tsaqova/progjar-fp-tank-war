package common.clientcomm;

import common.abstraction.AComm;
import common.component.Status;
import common.config.CommMessage;

public class ClientCommLogout extends AComm {

	private String userName;
	private String roomName;
	private Status status;
	
	public ClientCommLogout(String userName, String roomName, Status status) {
		setMsg(CommMessage.LOGOUT);
		setUserName(userName);
		setRoomName(roomName);
		setStatus(status);
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
}
