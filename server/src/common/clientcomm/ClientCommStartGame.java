package common.clientcomm;

import common.abstraction.AComm;

public class ClientCommStartGame extends AComm {

	private String roomName;

	public ClientCommStartGame(String roomName) {
		setRoomName(roomName);
	}
	
	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	
}
