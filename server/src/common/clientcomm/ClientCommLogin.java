package common.clientcomm;

import common.abstraction.AComm;
import common.config.CommMessage;

public class ClientCommLogin extends AComm {

	private String username;

	public ClientCommLogin(String username) {
		setMsg(CommMessage.LOGIN);
		setUsername(username);
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
}
