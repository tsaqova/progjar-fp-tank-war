package common.clientcomm;

import common.abstraction.AComm;

public class ClientCommShoot extends AComm {

	private String username;
	private int direction;
	private String roomName;
	
	public ClientCommShoot(String username, int direction, String roomName) {
		setUsername(username);
		setDirection(direction);
		setRoomName(roomName);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

}
