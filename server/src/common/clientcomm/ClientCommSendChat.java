package common.clientcomm;

import common.abstraction.AComm;
import common.component.Chat;

public class ClientCommSendChat extends AComm {

	private Chat chat;

	public ClientCommSendChat(Chat chat) {
		setChat(chat);
	}
	
	public Chat getChat() {
		return chat;
	}

	public void setChat(Chat chat) {
		this.chat = chat;
	}
	
}
