package common.component;

import java.awt.Graphics2D;
import java.awt.Image;
import java.io.Serializable;
import java.util.List;

import javax.swing.ImageIcon;

import common.abstraction.AObject;

public class Missile extends AObject implements Serializable  {

	private float xa = 0;
	private float ya = 0;
	int cooldown = 100;
	private int direction;
	
	public float getXa() {
		return xa;
	}

	public void setXa(float xa) {
		this.xa = xa;
	}

	public float getYa() {
		return ya;
	}

	public void setYa(float ya) {
		this.ya = ya;
	}
	
	public Missile(float x, float y,int direction)
	{
		setX(x);
		setY(y);
		this.setStatus(true);
		if(direction == 1)
			setXa((float) -2);
		else if(direction == 2)
			setXa((float)2);
		else if(direction == 3)
			setYa((float)-2);
		else if(direction == 4)
			setYa((float)2);
		setHeight(10);
		setWidth(10);
	}
	
	@Override
	public void paint(Graphics2D g) {
		ImageIcon ii = new ImageIcon("images/missile-user.png");
		g.drawImage(ii.getImage(), (int)getX(), (int)getY(), null);
	}
	
	@Override
	public void update(List<AObject> listObjects) 
	{
		for(AObject object : listObjects) {
			if(collision(object)) {
				this.setStatus(false);
				return;
			}
		}
		move();
	}

	public void move() 
	{	
		setX(getX() + xa);
		setY(getY() + ya);
	}
	
	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return null;
	}
}
