package common.component;

import java.awt.Graphics2D;
import java.awt.Image;
import java.io.Serializable;
import java.util.List;

import javax.swing.ImageIcon;

import common.abstraction.AObject;

public class Block extends AObject  implements Serializable  {
	
	private Image brick;
	public Block(float x, float y){
		setStatus(true);
		setX(x);
		setY(y);
		setHeight(176);
		setWidth(36);
		loadImage();
	}
	
	
	@Override
	public void paint(Graphics2D g) {
		g.drawImage(brick, (int)getX(), (int)getY(), null);
		// TODO Auto-generated method stub
		
	}
	
	private void loadImage() {
        ImageIcon ii = new ImageIcon("images/brick.png");
        brick = ii.getImage();        
    }


	@Override
	public void update(List<AObject> listObjects) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return null;
	}
		
}
