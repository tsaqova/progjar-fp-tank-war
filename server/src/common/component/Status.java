package common.component;

import java.io.Serializable;

public enum Status  implements Serializable  {

	LOBBY, TEAMSELECT, INGAME, DISCONNECT 
	
}
