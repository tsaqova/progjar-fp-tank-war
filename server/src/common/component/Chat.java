package common.component;

import java.io.Serializable;

public class Chat implements Serializable {

	private String sender;
	private String to;
	private String message;
	
	private Chat() {
		this.setTo("All");
	}
	
	public Chat(String sender, String message) {
		this();
		setSender(sender);
		setMessage(message);
	}
	
//	public Chat(String sender, String to, String message) {
//		setSender(sender);
//		setTo(to);
//		setMessage(message);
//	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
