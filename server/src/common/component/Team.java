package common.component;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Team implements Serializable {

	private Room room;
	private String name;
	private HashMap<String, Player> listPlayer;
	
	public Team(String name, Room room) {
		setName(name);
		setRoom(room);
		this.listPlayer = new HashMap<String, Player>();
	}
	
	public Boolean addPlayer(Player player) throws IOException {
		if(!this.listPlayer.containsKey(player.getUsername()) && !isFull()) {
			this.listPlayer.put(player.getUsername(), player);
			this.room.broadCastTeam();
			return true;
		}
		return false;
	}
	
	public Boolean removePlayer(Player player) throws IOException {
		if(this.listPlayer.containsKey(player.getUsername())) {
			this.listPlayer.remove(player.getUsername());
			this.room.broadCastTeam();
			return true;
		}
		return false;
	}
	
	public Boolean removePlayer(String name) throws IOException {
		if(this.listPlayer.containsKey(name)) {
			this.listPlayer.remove(name);
			this.room.broadCastTeam();
			return true;
		}
		return false;
	}
	
	public Player getPlayer(String username) {
		if(this.listPlayer.containsKey(username)) {
			return this.listPlayer.get(username);
		}
		return null;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Boolean isFull() {
		return this.listPlayer.size() >= 4;
	}
	
	public List<Player> getListPlayer() {
		List<Player> listPlayers = new ArrayList<Player>();
		for (Map.Entry<String, Player>entry : this.listPlayer.entrySet()) {
			listPlayers.add(entry.getValue());
		}
		return listPlayers;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}
	
}
