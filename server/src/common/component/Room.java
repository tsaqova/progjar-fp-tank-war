package common.component;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import server.TankWarServer;
import server.component.CommThread;
import server.component.Lobby;
import server.component.PassiveThread;
import common.abstraction.AObject;
import common.servercomm.ServerCommLeaveRoom;
import common.servercomm.ServerCommRefreshTeam;

public class Room implements Serializable, Runnable {

	private String roomName;
	private Player roomMaster;
	private HashMap<String, Team> listTeam;
	private Thread thread;
	private List<AObject> listAObjects;
	
	public Room(String roomName, Player roomMaster) throws IOException {
		setListAObjects(new ArrayList<AObject>());
		this.listTeam = new HashMap<String, Team>();
		setName(roomName);
		setRoomMaster(roomMaster);
		addTeam(new Team("Team A", this));
		addTeam(new Team("Team B", this));
		roomMaster.setTeamName("Team A");
		roomMaster.setRoomName(roomName);
		getTeam("Team A").addPlayer(roomMaster);
	}
	
	public String getName() {
		return roomName;
	}

	public void setName(String name) {
		this.roomName = name;
	}
	
	public Player getRoomMaster() {
		return roomMaster;
	}

	public void setRoomMaster(Player roomMaster) {
		this.roomMaster = roomMaster;
	}
	
	public Boolean addTeam(Team team) {
		if(!this.listTeam.containsKey(team.getName())) {
			this.listTeam.put(team.getName(), team);
			return true;
		}
		return false;
	}
	
	public Boolean removeTeam(Team team) {
		if(this.listTeam.containsKey(team.getName())) {
			this.listTeam.remove(team.getName());
			return true;
		}
		return false;
	}
	
	public Team getTeam(String name) {
		if(this.listTeam.containsKey(name)) {
			return this.listTeam.get(name);
		}
		return null;
	}
	
	public String addPlayer(Player player) throws IOException {
		if(getTeam("Team A").addPlayer(player)) {
			return "Team A";
		}
		else if(getTeam("Team B").addPlayer(player)) {
			return "Team B";
		}
		return "";
	}

	public HashMap<String, Team> getListTeam() {
		return listTeam;
	}

	public void setListTeam(HashMap<String, Team> listTeam) {
		this.listTeam = listTeam;
	}
	
	public List<Player> getListPlayer() {
		List<Player> listPlayers = new ArrayList<Player>();
		listPlayers.addAll(getTeam("Team A").getListPlayer());
		listPlayers.addAll(getTeam("Team B").getListPlayer());
		return listPlayers;
	}
	
	public void broadCastTeam() throws IOException {
		List<Player> listPlayersA = getTeam("Team A").getListPlayer();
		List<Player> listPlayersB = getTeam("Team B").getListPlayer();
		ServerCommRefreshTeam scrt = new ServerCommRefreshTeam(listPlayersA, listPlayersB);
		
		for (Player player : getListPlayer()) {
			CommThread commThread = TankWarServer.getInstance().getLoginManager().getCommThread(player.getUsername());
			commThread.getPassiveThread().getOos().writeObject(scrt);
			commThread.getPassiveThread().getOos().flush();
		}
	}
	
	public Boolean changeTeam(Player player, String teamName) throws IOException {
		if(!getTeam(teamName).isFull() && 
			getTeam(player.getTeamName()).removePlayer(player) &&
			getTeam(teamName).addPlayer(player)) {
			broadCastTeam();
			return true;
		}
		return false;
	}

	public Boolean removePlayer(String userName) throws IOException {
		if(getTeam("Team A").removePlayer(userName)) {
			if(getListPlayer().size() == 0) {
				TankWarServer.getInstance().getLobby().removeRoom(this.roomName);
			}
			return true;
		}
		else if(getTeam("Team B").removePlayer(userName)) {
			if(getListPlayer().size() == 0) {
				TankWarServer.getInstance().getLobby().removeRoom(this.roomName);
			}
			return true;
		}
		return false;
	}

	public boolean moveToLobby(String userName) throws IOException {
		if(removePlayer(userName)) {
			Lobby lobby = TankWarServer.getInstance().getLobby();
			lobby.addUser(userName);
			return true;
		}
		return false;
	}

	public void start() {
		this.thread = new Thread(this);
		this.thread.start();
	}

	private void update()
	{	
		Iterator<AObject> it = this.listAObjects.iterator();
		while (it.hasNext()==true) {
			AObject ao = it.next();
			if(ao.getStatus())
				ao.update(this.listAObjects);
			else
			{
				ao.setX(-1000);
				ao.setY(-1000);
				it.remove();
			}
		}
	}
	
	@Override
	public void run() {
		List<Player> listPlayer = getListPlayer();
		System.out.println("inisial game");
		initializeGame(listPlayer);
		System.out.println("total player : " + listPlayer.size());
		broadCastStartGame(listPlayer);
		while(!this.thread.isInterrupted()) {
			broadCastListObject(listPlayer);
			try {
				thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void broadCastListObject(List<Player> listPlayer) {
		for(Player player : listPlayer) {
			PassiveThread passiveThread = TankWarServer.getInstance().getLoginManager().getCommThread(player.getUsername()).getPassiveThread();
			try {
				passiveThread.sendListObject(getListAObjects());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void broadCastStartGame(List<Player> listPlayer) {
		for(Player player : listPlayer) {
			System.out.println("mengirim pesan start game ke user : " + player.getUsername());
			PassiveThread passiveThread = TankWarServer.getInstance().getLoginManager().getCommThread(player.getUsername()).getPassiveThread();
			try {
				passiveThread.sendStartGame();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void initializeGame(List<Player> listPlayer) {
		float posya = 0;
		float posyb = 0;
		for(Player player : listPlayer) {
			int x;
			int y;
			if(player.getTeamName().equals("Team A")) {
				addObject(new Tank((float)0.0, posya, player.getTeamName(), player.getUsername()));
				posya = posya + 100;
			}
			else if(player.getTeamName().equals("Team B")) {
				addObject(new Tank((float)500.0, posyb, player.getTeamName(), player.getUsername()));
				posyb = posyb + 100;
			}
		}
	}

	public void addObject(AObject object) {
		this.listAObjects.add(object);
	}
	
	public void removeObject(AObject object) {
		this.listAObjects.remove(object);
	}
	
	public void fireMissile(Tank tank, int direction){
//		Missile missile = null;
		if(direction == 2){
			addObject(new Missile(tank.getX()+60,tank.getY()+20, direction));
		}
		else if(direction == 1)
			addObject(new Missile(tank.getX()-20, tank.getY()+20,direction));
		else if(direction == 3)
			addObject(new Missile(tank.getX()+20, tank.getY()-20,direction));
		else if(direction == 4)
			addObject(new Missile(tank.getX()+20, tank.getY()+60,direction));
			}

	public List<AObject> getListAObjects() {
		return listAObjects;
	}

	public void setListAObjects(List<AObject> listAObjects) {
		this.listAObjects = listAObjects;
	}

	public void moveObjects(String username, int direction) {
		//System.out.println("menjalankan move object ke " + username);
		AObject object = null;
		for(AObject ao : this.listAObjects) {
			if(ao instanceof Tank) {
				if(((Tank) ao).getUsername().equals(username)) {
					((Tank)ao).move(direction);
				}
			}
		}
	}

	public void shootObjects(String username, Room room, int direction) {
		Iterator<AObject> it = getListAObjects().iterator();
		while(it.hasNext()) {
			if(it instanceof Tank) {
				if(((Tank) it).getUsername().equals(username)) {
					((Tank)it).shoot(username, room, direction);
				}
			}
		}
	}
	
}
