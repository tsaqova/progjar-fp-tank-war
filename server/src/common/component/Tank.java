package common.component;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.io.Serializable;
import java.util.List;

import javax.swing.ImageIcon;

import server.TankWarServer;
import common.abstraction.AObject;
import common.config.Config;

public class Tank extends AObject  implements Serializable {

	private float xa = 0;
	private float ya = 0;
	private int direction;
	//private Image imagetank;
	int cooldown;
	private int type;
	private String teamName;
	private String username;

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public Tank(float x, float y, String teamName, String userName)
	{
		this.setStatus(true);
		this.cooldown = 0;
		setDirection(1);
		setHeight(50);
		setWidth(50);
		this.teamName = teamName;
		this.username = userName;
	}

	public void update(List<AObject> listObjects) 
	{
		for(AObject object : listObjects) {
			if(object instanceof Missile) {
				this.setStatus(false);
			}
			else if(object instanceof Block) {
				if(collision(object))
				{
					if(direction == 1)
						setX(getX()+2);
					else if(direction == 2)
						setX(getX()-2);
					else if(direction == 3)
						setY(getY()+2);
					else if(direction == 4)
						setY(getY()-2);
					return;
				}
			}
		}
	}

	public void move(int direction) {
		setDirection(direction);
		this.xa = this.ya = 0;
		if(direction == 1)
			this.xa = -1;
		else if(direction == 2)
			this.xa = 1;
		else if(direction == 3)
			this.ya = -1;
		else if(direction == 4)
			this.ya = 1;
		if (getX() + xa > 0 && getX() + xa < Config.WINDOW_WIDTH-50)
			setX(getX() + xa);
		if (getY() + ya > 0 && getY() + ya < Config.WINDOW_HEIGHT-50)
			setY(getY() + ya);
		System.out.println("koordinat " + getUsername() + " : " + getX() + " " + getY());
	}

	public void paint(Graphics2D g) {
		if(this.getStatus()){
			ImageIcon imagetank = null;
			if(direction==1) {
				imagetank = new ImageIcon("images/tank-user-left.png");
				direction = 1;
			}
			else if(direction==2) {
				imagetank = new ImageIcon("images/tank-user-right.png");
				direction = 2;
			}
			else if(direction==3) {
				imagetank = new ImageIcon("images/tank-user-up.png");
				direction = 3;
			}
			else if(direction==4) {
				imagetank = new ImageIcon("images/tank-user-down.png");
				direction = 4;
			}
			//g.fillRect((int)getX(), (int)getY(), 20, 20);
			g.drawImage(imagetank.getImage(), (int)getX(), (int)getY(), null);
			this.cooldown = this.cooldown -1;
		}
	}

	public void keyReleased(KeyEvent e) {
		this.xa = 0;
		this.ya = 0;
		//masih bug
		if(e.getKeyCode() == 37 ||
				e.getKeyCode() == 38 ||
				e.getKeyCode() == 39 ||
				e.getKeyCode() == 40) {
		}
	}

	public void shoot(String username2, Room room, int direction2) {
		if(this.cooldown <= 0){
			this.cooldown = 100;
			Room room1 = TankWarServer.getInstance().getLobby().getRoom(room.getName());
			Missile missile = null;
			if(direction == 2)
				missile = new Missile(getX()+60,getY()+20,direction);
			else if(direction == 1)
				missile = new Missile(getX()-20,getY()+20,direction);
			else if(direction == 3)
				missile = new Missile(getX()+20,getY()-20,direction);
			else if(direction == 4)
				missile = new Missile(getX()+20,getY()+60,direction);
			room1.addObject(missile);
		}
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
