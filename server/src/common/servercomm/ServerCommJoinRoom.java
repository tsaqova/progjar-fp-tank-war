package common.servercomm;

import common.abstraction.AComm;

public class ServerCommJoinRoom extends AComm {

	private String teamName;

	public ServerCommJoinRoom(String teamName) {
		setTeamName(teamName);
	}
	
	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	
}
