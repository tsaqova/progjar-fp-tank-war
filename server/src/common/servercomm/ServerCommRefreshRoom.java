package common.servercomm;

import java.util.List;

import common.abstraction.AComm;
import common.component.Room;
import common.config.CommMessage;

public class ServerCommRefreshRoom extends AComm {

	private List<Room> listRoom;
	
	public ServerCommRefreshRoom(List<Room> listRoom) {
		setMsg(CommMessage.REFRESHROOM);
		setListRoom(listRoom);
	}

	public List<Room> getListRoom() {
		return listRoom;
	}

	public void setListRoom(List<Room> listRoom) {
		this.listRoom = listRoom;
	}
	
}
