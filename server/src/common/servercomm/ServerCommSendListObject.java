package common.servercomm;

import java.util.List;

import common.abstraction.AComm;
import common.abstraction.AObject;

public class ServerCommSendListObject extends AComm {

	private List<AObject> listObject;

	public ServerCommSendListObject(List<AObject> listObject) {
		setListObject(listObject);
	}
	
	public List<AObject> getListObject() {
		return listObject;
	}

	public void setListObject(List<AObject> listObject) {
		this.listObject = listObject;
	}

}
