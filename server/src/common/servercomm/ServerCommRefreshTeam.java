package common.servercomm;

import java.util.List;

import common.abstraction.AComm;
import common.component.Player;

public class ServerCommRefreshTeam extends AComm {

	public List<Player> listPlayersA;
	public List<Player> listPlayersB;
	
	public ServerCommRefreshTeam(List<Player> listPlayersA, List<Player> listPlayersB) {
		setListPlayerA(listPlayersA);
		setListPlayerB(listPlayersB);
	}
	
	public List<Player> getListPlayersA() {
		return listPlayersA;
	}
	public void setListPlayerA(List<Player> listPlayersA) {
		this.listPlayersA = listPlayersA;
	}
	public List<Player> getListPlayersB() {
		return listPlayersB;
	}
	public void setListPlayerB(List<Player> listPlayersB) {
		this.listPlayersB = listPlayersB;
	}

}
