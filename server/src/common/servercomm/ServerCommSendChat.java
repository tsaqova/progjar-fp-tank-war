package common.servercomm;

import common.abstraction.AComm;
import common.component.Chat;

public class ServerCommSendChat extends AComm {

	private Chat chat;

	public ServerCommSendChat(Chat chat) {
		setChat(chat);
	}
	
	public Chat getChat() {
		return chat;
	}

	public void setChat(Chat chat) {
		this.chat = chat;
	}
	
}
