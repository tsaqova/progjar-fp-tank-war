package common.servercomm;

import java.util.List;

import common.abstraction.AComm;
import common.component.Player;

public class ServerCommRefreshUser extends AComm {

	private List<Player> listPlayers;
	
	public ServerCommRefreshUser(List<Player> listPlayers) {
		setListUsers(listPlayers);
	}

	public List<Player> getListUsers() {
		return listPlayers;
	}

	public void setListUsers(List<Player> listPlayers2) {
		this.listPlayers = listPlayers2;
	}
	
}
