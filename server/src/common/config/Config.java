package common.config;

public final class Config {

	public static final int PORT_ACTIVE = 6666;
	public static final int PORT_PASSIVE = 6667;
	
	public static final int WINDOW_HEIGHT = 600;
	public static final int WINDOW_WIDTH = 800;
}
