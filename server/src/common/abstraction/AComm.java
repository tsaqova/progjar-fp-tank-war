package common.abstraction;

import java.io.Serializable;

import common.config.CommMessage;

public abstract class AComm implements Serializable {

	private CommMessage msg;
	
	public AComm() {
		setMsg(CommMessage.NOTSET);
	}

	public CommMessage getMsg() {
		return msg;
	}

	public void setMsg(CommMessage msg) {
		this.msg = msg;
	}
	
}
