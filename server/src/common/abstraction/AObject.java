package common.abstraction;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.io.Serializable;
import java.util.List;

public abstract class AObject implements Serializable {
	
	private boolean status= false;
	private int width;
	private int height;
	float x;
	float y;
	
	abstract public void update(List<AObject> listObjects);
	abstract public void paint(Graphics2D g);
	
	public float getX() {
		return x;
	}
	public void setX(float x) {
		this.x = x;
	}
	public float getY() {
		return y;
	}
	public void setY(float y) {
		this.y = y;
	}
	
	public void remove()
	{
		try {
			this.finalize();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean getStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	public Rectangle getBound(){
		return new Rectangle((int)getX()-5, (int)getY()-5, getWidth()+5, getHeight()+5);
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	
	public boolean collision(AObject object) {
		if (object.getBound().intersects(this.getBound())){
			return true;
		}
		else return false;
	}
	public abstract String getUsername();

}
