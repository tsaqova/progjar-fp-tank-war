package client.scene;

import common.component.Player;
import client.abstraction.AScene;
import client.actionevent.LeaveRoomActionEvent;
import client.actionevent.StartGameActionEvent;
import client.actionevent.ToTeamAActionEvent;
import client.actionevent.ToTeamBActionEvent;
import client.component.GButton;
import client.component.TeamPanel;
import client.component.TeamPanelRenderer;

public class TeamMenu extends AScene {

	private GButton startGameButton;
	private GButton teamAButton;
	private GButton teamBButton;
	private GButton leaveRoomButton;


	private TeamPanel teamAPanel;
	private TeamPanel teamBPanel;

	public TeamMenu() {
		setStartGameButton(new GButton("Start Game", new StartGameActionEvent()));
		setLeaveRoomButton(new GButton("Leave Room", new LeaveRoomActionEvent()));
		setTeamAButton(new GButton("Team A", new ToTeamAActionEvent()));
		setTeamBButton(new GButton("Team B", new ToTeamBActionEvent()));
		setTeamAPanel(new TeamPanel(new TeamPanelRenderer()));
		setTeamBPanel(new TeamPanel(new TeamPanelRenderer()));
		getLeaveRoomButton().setBounds(200, 450, 120, 35);
		getStartGameButton().setBounds(350, 450, 100, 35);
		getTeamAButton().setBounds(90, 50, 90, 35);
		getTeamBButton().setBounds(580, 50, 90, 35);
		getTeamAPanel().setBounds(40, 100, 200, 300);
		getTeamBPanel().setBounds(520, 100, 200, 300);
		add(startGameButton);
		add(leaveRoomButton);
		add(teamAButton);
		add(teamBButton);
		add(teamAPanel);
		add(teamBPanel);
	}

	public GButton getStartGameButton() {
		return startGameButton;
	}

	public void setStartGameButton(GButton startGameButton) {
		this.startGameButton = startGameButton;
	}

	public TeamPanel getTeamAPanel() {
		return teamAPanel;
	}

	public void setTeamAPanel(TeamPanel teamAPanel) {
		this.teamAPanel = teamAPanel;
	}

	public TeamPanel getTeamBPanel() {
		return teamBPanel;
	}

	public void setTeamBPanel(TeamPanel teamBPanel) {
		this.teamBPanel = teamBPanel;
	}

	public GButton getTeamAButton() {
		return teamAButton;
	}

	public void setTeamAButton(GButton teamAButton) {
		this.teamAButton = teamAButton;
	}

	public GButton getTeamBButton() {
		return teamBButton;
	}

	public void setTeamBButton(GButton teamBButton) {
		this.teamBButton = teamBButton;
	}

	public GButton getLeaveRoomButton() {
		return leaveRoomButton;
	}

	public void setLeaveRoomButton(GButton leaveRoomButton) {
		this.leaveRoomButton = leaveRoomButton;
	}
	
}
