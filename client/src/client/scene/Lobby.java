package client.scene;

import javax.swing.JLabel;

import common.component.Chat;
import client.abstraction.AScene;
import client.actionevent.CreateRoomActionEvent;
import client.actionevent.JoinRoomActionEvent;
import client.component.ChatBox;
import client.component.GButton;
import client.component.ListRoomPanel;
import client.component.ListRoomRenderer;
import client.component.TeamPanel;
import client.component.TeamPanelRenderer;

public class Lobby extends AScene {
	
	private GButton createRoomButton;
	private GButton joinRoomButton;
	private ListRoomPanel listRoomPanel;
	private JLabel listRoomLabel;
	private ChatBox chatBox;
	private JLabel chatRoomLabel;
	private JLabel enterMessageLabel;
	private TeamPanel listLobbyUser;
	private JLabel userInLobbyLabel;
	
	public Lobby() {
		
		setListRoomLabel(new JLabel("List Room"));
		getListRoomLabel().setBounds(50, 10, 100, 20);
		
		setListRoomPanel(new ListRoomPanel(new ListRoomRenderer()));
		getListRoomPanel().setBounds(50, 40, 350, 440);
		
		setCreateRoomButton(new GButton("Create Room", new CreateRoomActionEvent()));
		getCreateRoomButton().setBounds(50, 495, 120, 40);
		
		setJoinRoomButton(new GButton("Join Room", new JoinRoomActionEvent()));
		getJoinRoomButton().setBounds(275, 495, 120, 40);
		
		setUserInLobbyLabel(new JLabel("Player in lobby : "));
		getUserInLobbyLabel().setBounds(500, 10, 100, 20);
		
		setListLobbyUser(new TeamPanel(new TeamPanelRenderer()));
		getListLobbyUser().setBounds(500, 40, 200, 220);
		
		setChatRoomLabel(new JLabel("Chat Room"));
		getChatRoomLabel().setBounds(500, 270, 100, 20);
		
		setEnterMessageLabel(new JLabel("Enter message to send : "));
		getEnterMessageLabel().setBounds(500, 397, 200, 20);
		
		setChatBox(new ChatBox());
		getChatBox().getMessageScrollBox().setBounds(500, 290, 200, 100);
		getChatBox().getTypeBox().setBounds(500, 425, 200, 50);
		
		
		add(listRoomLabel);
		add(listRoomPanel);
		add(createRoomButton);
		add(joinRoomButton);
		add(chatBox.getTypeBox());
		add(chatBox.getMessageScrollBox());
		add(chatRoomLabel);
		add(enterMessageLabel);
		add(userInLobbyLabel);
		add(listLobbyUser);
	}
	
	public void receiveChat(Chat chat) {
		String rcvMessage = chat.getSender() + " : " + chat.getMessage() + "\n";
		chatBox.getMessageBox().append(rcvMessage);
		chatBox.getMessageBox().setCaretPosition(chatBox.getMessageBox().getDocument().getLength());
	}
	
	public ListRoomPanel getListRoomPanel() {
		return listRoomPanel;
	}

	public void setListRoomPanel(ListRoomPanel listRoomPanel) {
		this.listRoomPanel = listRoomPanel;
	}
	
	public GButton getCreateRoomButton() {
		return createRoomButton;
	}

	public void setCreateRoomButton(GButton createRoomButton) {
		this.createRoomButton = createRoomButton;
	}
	
	public GButton getJoinRoomButton() {
		return joinRoomButton;
	}

	public void setJoinRoomButton(GButton joinRoomButton) {
		this.joinRoomButton = joinRoomButton;
	}
	
	private JLabel getListRoomLabel() {
		return listRoomLabel;
	}

	private void setListRoomLabel(JLabel listRoomLabel) {
		this.listRoomLabel = listRoomLabel;
	}

	public ChatBox getChatBox() {
		return chatBox;
	}

	public void setChatBox(ChatBox chatBox) {
		this.chatBox = chatBox;
	}

	private JLabel getChatRoomLabel() {
		return chatRoomLabel;
	}

	private void setChatRoomLabel(JLabel chatRoomLabel) {
		this.chatRoomLabel = chatRoomLabel;
	}

	private JLabel getEnterMessageLabel() {
		return enterMessageLabel;
	}

	private void setEnterMessageLabel(JLabel enterMessageLabel) {
		this.enterMessageLabel = enterMessageLabel;
	}

	public TeamPanel getListLobbyUser() {
		return listLobbyUser;
	}

	public void setListLobbyUser(TeamPanel listLobbyUser) {
		this.listLobbyUser = listLobbyUser;
	}

	public JLabel getUserInLobbyLabel() {
		return userInLobbyLabel;
	}

	public void setUserInLobbyLabel(JLabel userInLobbyLabel) {
		this.userInLobbyLabel = userInLobbyLabel;
	}
	
}
