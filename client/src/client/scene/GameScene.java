package client.scene;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import common.abstraction.AObject;
import common.component.Block;
import client.Client;
import client.abstraction.AScene;

public class GameScene extends AScene implements Runnable {
	
	private Thread thread;
	private List<AObject> listOfObject = new ArrayList<AObject>();
	private List<Block> listOfBlock = new ArrayList<Block>();
	public Object object;
	public GameScene()
	{
		addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {

			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				System.out.println("keypresed");
				if (e.getKeyCode() == KeyEvent.VK_LEFT)
				{
					Client client = Client.getInstance();
					try {
						client.getActiveThread().sendMove(client.getUserName(), 1);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				if (e.getKeyCode() == KeyEvent.VK_RIGHT)
				{
					Client client = Client.getInstance();
					try {
						client.getActiveThread().sendMove(client.getUserName(), 2);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				if (e.getKeyCode() == KeyEvent.VK_UP)
				{
					Client client = Client.getInstance();
					try {
						client.getActiveThread().sendMove(client.getUserName(), 3);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				if (e.getKeyCode() == KeyEvent.VK_DOWN)
				{
					Client client = Client.getInstance();
					try {
						client.getActiveThread().sendMove(client.getUserName(), 4);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				if (e.getKeyCode() == KeyEvent.VK_SPACE)
				{
					Client client = Client.getInstance();
					try {
						client.getActiveThread().sendShoot(client.getUserName(), client.getDirection());
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		setListOfObject(new ArrayList<AObject>());
		setBackground(Color.BLACK);
		start();
	}
	
	public void start() {
		thread = new Thread(this);
		thread.start();
	}
	
	@Override
	public void paint(Graphics g)
	{
		//System.out.println("repaint object dengan k : " + getListOfObject().size());
		super.paint(g);		
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
		for(AObject object : getListOfObject()){
			//System.out.println("paint object " + object.getX() + " " + object.getY());
			object.paint(g2d);
		}
	}


	public void addObject(AObject a){
		getListOfObject().add(a);
	}
	
	public void removeObject(AObject a){
		getListOfObject().remove(a);
	}

	public List<AObject> getListOfObject() {
		return listOfObject;
	}

	public void setListOfObject(List<AObject> listOfObject) {
		this.listOfObject = listOfObject;
	}

	@Override
	public void run() {
		while(true){
			this.repaint();
		}
	}
	
}
