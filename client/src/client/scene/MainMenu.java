package client.scene;

import java.awt.Dimension;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;

import javax.swing.JLabel;
import javax.swing.JTextField;

import client.Client;
import client.abstraction.AScene;
import client.actionevent.ConnectToServerActionEvent;
import client.component.GButton;

public class MainMenu extends AScene {

	private JTextField usernameField;
	private GButton playButton;
	private JLabel usernameLabel;

	public MainMenu() {
		setFocusable(true);
		requestFocusInWindow();
		
		setUsernameLabel(new JLabel("Username : "));
		getUsernameLabel().setBounds(200, 240, 200, 50);
		setUsernameField(new JTextField());
		getUsernameField().setBounds(300, 250, 200, 40);
		getUsernameField().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				getPlayButton().actionPerformed(e);
			}
		});
		setPlayButton(new GButton("Play", new ConnectToServerActionEvent()));
		getPlayButton().setBounds(300, 300, 200, 50);
		add(usernameLabel);
		add(usernameField);
		add(playButton);
	}
	
	public GButton getPlayButton() {
		return playButton;
	}

	private void setPlayButton(GButton playButton) {
		this.playButton = playButton;
	}
	
	public JTextField getUsernameField() {
		return usernameField;
	}

	private void setUsernameField(JTextField usernameField) {
		this.usernameField = usernameField;
	}
	
	public JLabel getUsernameLabel() {
		return usernameLabel;
	}

	private void setUsernameLabel(JLabel usernameLabel) {
		this.usernameLabel = usernameLabel;
	}
	
}
