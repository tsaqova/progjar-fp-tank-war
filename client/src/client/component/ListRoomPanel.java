package client.component;

import java.awt.Dimension;

import javax.swing.JScrollPane;

import common.component.Room;

import client.abstraction.AListPanel;
import client.abstraction.AListRenderer;

public class ListRoomPanel extends AListPanel<Room> {

	public ListRoomPanel(AListRenderer<Room> listRenderer) {
		super(listRenderer);
		setPreferredSize(new Dimension(250, 250));
	}

}
