
package client.component;

import java.awt.Container;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.JFrame;

import common.component.Status;

import client.Client;
import client.abstraction.AScene;
import client.config.Config;
import client.scene.GameScene;
import client.scene.Lobby;
import client.scene.MainMenu;
import client.scene.TeamMenu;

public class Window extends JFrame {
	
	private static Window instance = new Window("Tank Wars");
	private AScene activeScene;
	private MainMenu mainMenu;
	private Lobby lobby;
	private TeamMenu teamMenu;
	private GameScene gameScene;

	private Window() {
		setSize(common.config.Config.WINDOW_WIDTH, common.config.Config.WINDOW_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e)
			{
				try {
					if(!Client.getInstance().getStatus().equals(Status.DISCONNECT)) {
						Client.getInstance().getActiveThread().disconnect();
					}
				} catch (ClassNotFoundException | IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		
		setMainMenu(new MainMenu());
		setLobby(new Lobby());
		setTeamMenu(new TeamMenu());
		setGameScene(new GameScene());

		changeScene(getMainMenu());
		getGameScene().start();
	}

	private Window(String title) {
		this();
		setTitle(title);
	}
	
	public static Window getInstance() {
		return instance;
	}
	
	public void refreshScene() {
		setContentPane((Container) this.activeScene);
	}
	
	public AScene getActiveScene() {
		return this.activeScene;
	}
	
	public void changeScene(AScene scene) {
		setActiveScene(scene);
		refreshScene();
		scene.setFocusable(true);
		scene.requestFocus();
	}
	
	public void setActiveScene(AScene scene) {
		this.activeScene = scene;
	}
	
	public MainMenu getMainMenu() {
		return mainMenu;
	}

	public void setMainMenu(MainMenu mainMenu) {
		this.mainMenu = mainMenu;
	}
	
	public Lobby getLobby() {
		return lobby;
	}

	public void setLobby(Lobby lobby) {
		this.lobby = lobby;
	}
	
	public TeamMenu getTeamMenu() {
		return teamMenu;
	}

	public void setTeamMenu(TeamMenu teamMenu) {
		this.teamMenu = teamMenu;
	}
	
	public GameScene getGameScene() {
		return gameScene;
	}

	public void setGameScene(GameScene gameScene) {
		this.gameScene = gameScene;
	}
	
}
