package client.component;

import client.abstraction.AButton;
import client.abstraction.IActionEvent;

public class GButton extends AButton {

	private GButton() {
		
	}
	
	private GButton(String text) {
		this();
		setText(text);
	}
	
	public GButton(String text, IActionEvent actionEvent) {
		this(text);
		setActionEvent(actionEvent);
	}
	
}
