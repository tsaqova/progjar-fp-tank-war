package client.component;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JList;

import common.component.Player;
import common.component.Room;
import client.abstraction.AListRenderer;

public class TeamPanelRenderer extends AListRenderer<Player> {
	
	@Override
	public Component getListCellRendererComponent(JList<? extends Player> list,
			Player value, int index, boolean isSelected, boolean cellHasFocus) {
		Player player = (Player) value;
		setText(player.getUsername());
		if (isSelected) {
			setBackground(Color.blue);
			setForeground(Color.white);
		} else {
			setBackground(Color.white);
			setForeground(Color.black);
		}
		return this;
	}

}
