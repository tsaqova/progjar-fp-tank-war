package client.component;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JList;

import common.component.Room;

import client.abstraction.AListRenderer;

public class ListRoomRenderer extends AListRenderer<Room> {
	
	@Override
	public Component getListCellRendererComponent(JList<? extends Room> list, Room value,
			int index, boolean isSelected, boolean cellHasFocus) {
		Room room = (Room) value;
		setText(room.getName());
		if (isSelected) {
			setBackground(Color.blue);
			setForeground(Color.white);
		} else {
			setBackground(Color.white);
			setForeground(Color.black);
		}
		return this;
	}

}
