package client.component;

import java.awt.Dimension;

import common.component.Player;
import client.abstraction.AListPanel;
import client.abstraction.AListRenderer;

public class TeamPanel extends AListPanel<Player>{ 
	
	public TeamPanel(AListRenderer<Player> listRenderer){
		super(listRenderer);
		setPreferredSize(new Dimension(250, 250));
	}
	
}
