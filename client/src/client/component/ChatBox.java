package client.component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import client.Client;
import common.clientcomm.ClientCommSendChat;
import common.component.Chat;

public class ChatBox {
	
	private JTextField typeBox;
	private JTextArea messageBox;
	private JScrollPane messageScrollBox;
	
	public ChatBox() {
		setTypeBox(new JTextField());
		setMessageBox(new JTextArea());
		messageBox.setLineWrap(true);
		messageBox.setWrapStyleWord(true);
		messageBox.setEditable(false);
		setMessageScrollBox(new JScrollPane(getMessageBox()));
		messageScrollBox.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		typeBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Client client = Client.getInstance();
				String message = typeBox.getText();
				Chat chat = new Chat(client.getUserName(), message);
				typeBox.setText("");
				try {
					client.getActiveThread().sendChat(chat);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
	}

	public JTextField getTypeBox() {
		return typeBox;
	}

	public void setTypeBox(JTextField typeBox) {
		this.typeBox = typeBox;
	}

	public JTextArea getMessageBox() {
		return messageBox;
	}

	public void setMessageBox(JTextArea messageBox) {
		this.messageBox = messageBox;
	}

	public JScrollPane getMessageScrollBox() {
		return messageScrollBox;
	}

	public void setMessageScrollBox(JScrollPane messageScrollBox) {
		this.messageScrollBox = messageScrollBox;
	}
	
}
