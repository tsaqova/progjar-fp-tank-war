package client.thread;

import java.awt.HeadlessException;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JOptionPane;

import client.Client;
import client.abstraction.AThread;
import client.component.Window;
import client.config.Config;
import client.config.Message;
import client.scene.MainMenu;
import common.clientcomm.ClientCommChangeTeam;
import common.clientcomm.ClientCommCreateRoom;
import common.clientcomm.ClientCommJoinRoom;
import common.clientcomm.ClientCommLeaveRoom;
import common.clientcomm.ClientCommLogin;
import common.clientcomm.ClientCommLogout;
import common.clientcomm.ClientCommMove;
import common.clientcomm.ClientCommSendChat;
import common.clientcomm.ClientCommShoot;
import common.clientcomm.ClientCommStartGame;
import common.component.Chat;
import common.component.Player;
import common.component.Room;
import common.component.Status;
import common.config.CommMessage;
import common.servercomm.ServerCommChangeTeam;
import common.servercomm.ServerCommCreateRoom;
import common.servercomm.ServerCommJoinRoom;
import common.servercomm.ServerCommLeaveRoom;
import common.servercomm.ServerCommLogin;
import common.servercomm.ServerCommLogout;

public class ActiveThread extends AThread {

	public ActiveThread(Client client) throws UnknownHostException, IOException {
		setClient(client);
	}

	public void run() {
		try {
			String username = Window.getInstance().getMainMenu().getUsernameField().getText();
			if(login(username)) {
				PassiveThread passiveThread = getClient().getPassiveThread();
				passiveThread.start();
				Client.getInstance().setUserName(username);
				while(!getThread().isInterrupted()) {
					// Waiting event on Window GUI
				}
				Client.getInstance().setStatus(Status.DISCONNECT);
				closeSocket();	
			}
			else {
				JOptionPane.showMessageDialog(Window.getInstance().getContentPane(), Message.NAME_ALREADY_USED);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (HeadlessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Boolean login(String username) throws IOException, ClassNotFoundException {
		setSocket(new Socket(Config.HOST, common.config.Config.PORT_ACTIVE));
		setOos(new ObjectOutputStream(getSocket().getOutputStream()));
		setOis(new ObjectInputStream(getSocket().getInputStream()));
		ClientCommLogin ccl = new ClientCommLogin(username);
		getOos().writeObject(ccl);
		getOos().flush();
		ServerCommLogin scl = (ServerCommLogin) getOis().readObject();
		if(scl.getMsg().equals(common.config.CommMessage.OK)) {
			return true;
		}
		else {
			return false;
		}
	}

	public void connect() {
		MainMenu mainMenu = Window.getInstance().getMainMenu();
		if(mainMenu.getUsernameField().getText().equals("")) {
			JOptionPane.showMessageDialog(Window.getInstance().getContentPane(), Message.USERNAME_EMPTY);
			return;
		}
		start();
	}

	public void disconnect() throws IOException, ClassNotFoundException {
		ServerCommLogout scl = new ServerCommLogout();
		try {
			ClientCommLogout ccl = new ClientCommLogout(getClient().getUserName(), getClient().getRoomName(), getClient().getStatus());
			getOos().writeObject(ccl);
			getOos().flush();
			scl = (ServerCommLogout) getOis().readObject();
		} catch (EOFException e) {
			
		} finally {
			if(scl.getMsg().equals(CommMessage.OK)) {
				getThread().interrupt();
			}
			else {
				JOptionPane.showMessageDialog(Window.getInstance().getContentPane(), Message.DISCONNECT_FAIL);
			}
		}
	}

	public void createRoom(String roomName) throws IOException, ClassNotFoundException {
		ServerCommCreateRoom sccr = new ServerCommCreateRoom();
		Client client = Client.getInstance();
		try {
			ClientCommCreateRoom cccr = new ClientCommCreateRoom(roomName);
			client.getActiveThread().getOos().writeObject(cccr);
			client.getActiveThread().getOos().flush();
			sccr = (ServerCommCreateRoom) client.getActiveThread().getOis().readObject();
		} catch (EOFException e) {
			
		} finally {
			if(sccr.getMsg().equals(CommMessage.OK)) {
				client.setRoomName(roomName);
				client.setTeamName("Team A");
				Window window = Window.getInstance();
				window.changeScene(window.getTeamMenu());
				client.setStatus(Status.TEAMSELECT);
			}
			else {
				JOptionPane.showMessageDialog(Window.getInstance().getContentPane(), Message.NAME_ALREADY_USED);
			}
		}
	}

	public void joinRoom(String roomName) {
		ServerCommJoinRoom scjr = new ServerCommJoinRoom("");
		try {
			Client client = Client.getInstance();
			ClientCommJoinRoom ccjr = new ClientCommJoinRoom(client.getUserName(), roomName);
			getOos().writeObject(ccjr);
			getOos().flush();
			scjr = (ServerCommJoinRoom) getOis().readObject();
			if (scjr.getMsg().equals(CommMessage.OK)){
				Window.getInstance().changeScene(Window.getInstance().getTeamMenu());
				client.setStatus(Status.TEAMSELECT);
				client.setRoomName(roomName);
				client.setTeamName(scjr.getTeamName());
			}
			else{
				JOptionPane.showMessageDialog(Window.getInstance().getContentPane(), Message.ROOM_IS_FULL);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void changeTeam(String teamName) throws IOException, ClassNotFoundException {
		ServerCommChangeTeam scct = new ServerCommChangeTeam();
		try {
			Client client = Client.getInstance();
			ClientCommChangeTeam ccct = new ClientCommChangeTeam(client.getUserName(), client.getRoomName(), client.getTeamName(), teamName);
			getOos().writeObject(ccct);
			getOos().flush();
			scct = (ServerCommChangeTeam) getOis().readObject();
		}	catch (EOFException e){

		}	finally {
			if(scct.getMsg().equals(CommMessage.OK)) {
				getClient().setTeamName(teamName);
			}
		}
	}

	public void sendChat(Chat chat) throws IOException {
		ClientCommSendChat ccsc = new ClientCommSendChat(chat);
		getOos().reset();
		getOos().writeObject(ccsc);
		getOos().flush();
	}

	public void leaveRoom() throws IOException, ClassNotFoundException {
		ServerCommLeaveRoom sccr = new ServerCommLeaveRoom();
		try {
			ClientCommLeaveRoom cclr = new ClientCommLeaveRoom(getClient().getUserName(), getClient().getRoomName());
			getOos().writeObject(cclr);
			getOos().flush();
			sccr = (ServerCommLeaveRoom) getOis().readObject();
		} catch(EOFException e) {
			
		} finally {
			if(sccr.getMsg().equals(CommMessage.OK)) {
				getClient().setTeamName(null);
				getClient().setRoomName(null);
				getClient().setStatus(Status.LOBBY);
				Window.getInstance().changeScene(Window.getInstance().getLobby());
			}
		}
	}

	public void startGame() throws IOException {
		ClientCommStartGame ccsg = new ClientCommStartGame(getClient().getRoomName());
		getOos().reset();
		getOos().writeObject(ccsg);
		getOos().flush();
	}
	
	public void sendMove(String username, int direction) throws IOException {
		System.out.println("mengirim perintah move ke server");
		ClientCommMove ccge= new ClientCommMove(username, direction, Client.getInstance().getRoomName());
		ccge.setMsg(CommMessage.MOVE);
		getOos().reset();
		getOos().writeObject(ccge);
		getOos().flush();
	}
	
	public void sendShoot(String username, int direction) throws IOException {
		System.out.println("mengirim perintah shoot ke server");
		ClientCommShoot ccge= new ClientCommShoot(username, direction, Client.getInstance().getRoomName());
		ccge.setMsg(CommMessage.SHOOT);
		getOos().reset();
		getOos().writeObject(ccge);
		getOos().flush();
	}
	
}