package client.thread;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;

import javax.swing.JOptionPane;

import common.abstraction.AComm;
import common.abstraction.AObject;
import common.clientcomm.ClientCommLogin;
import common.component.Chat;
import common.component.Player;
import common.component.Room;
import common.component.Status;
import common.config.CommMessage;
import common.config.Config;
import common.servercomm.ServerCommLeaveRoom;
import common.servercomm.ServerCommLogin;
import common.servercomm.ServerCommRefreshRoom;
import common.servercomm.ServerCommRefreshTeam;
import common.servercomm.ServerCommRefreshUser;
import common.servercomm.ServerCommSendChat;
import common.servercomm.ServerCommSendListObject;
import common.servercomm.ServerCommStartGame;
import client.Client;
import client.abstraction.AThread;
import client.component.Window;
import client.config.Message;
import client.scene.Lobby;
import client.scene.TeamMenu;

public class PassiveThread extends AThread {
	
	public PassiveThread(Client client) throws UnknownHostException, IOException {
		setClient(client);
	}
	
	public void run() {
		try {
			setSocket(new Socket(client.config.Config.HOST , Config.PORT_PASSIVE));
			setOos(new ObjectOutputStream(getSocket().getOutputStream()));
			setOis(new ObjectInputStream(getSocket().getInputStream()));
			ClientCommLogin ccl = new ClientCommLogin(getClient().getUserName());
			getOos().writeObject(ccl);
			getOos().flush();
			ServerCommLogin scl = new ServerCommLogin();
			scl = (ServerCommLogin) getOis().readObject();
			if(scl.getMsg().equals(CommMessage.OK)) {
				Window window = Window.getInstance();
				window.setActiveScene(window.getLobby());
				window.refreshScene();
				Client.getInstance().setStatus(Status.LOBBY);
				AComm comm;
				while((comm = (AComm) getOis().readObject()) != null) {
					if(comm instanceof ServerCommRefreshRoom) {
						List<Room> listRoom = ((ServerCommRefreshRoom) comm).getListRoom();
						Window.getInstance().getLobby().getListRoomPanel().refreshList(listRoom);
					}
					else if(comm instanceof ServerCommRefreshTeam) {
						List<Player> listPlayersA = ((ServerCommRefreshTeam) comm).getListPlayersA();
						List<Player> listPlayersB = ((ServerCommRefreshTeam) comm).getListPlayersB();
						TeamMenu teamMenu = Window.getInstance().getTeamMenu();
						teamMenu.getTeamAPanel().refreshList(listPlayersA);
						teamMenu.getTeamBPanel().refreshList(listPlayersB);
					}
					else if(comm instanceof ServerCommSendChat) {
						Lobby lobby = Window.getInstance().getLobby();
						lobby.receiveChat(((ServerCommSendChat) comm).getChat());
					}
					else if(comm instanceof ServerCommRefreshUser) {
						Lobby lobby = Window.getInstance().getLobby();
						lobby.getListLobbyUser().refreshList(((ServerCommRefreshUser) comm).getListUsers());
					}
					else if(comm instanceof ServerCommStartGame) {
						Window.getInstance().changeScene(Window.getInstance().getGameScene());
					}
					else if(comm instanceof ServerCommSendListObject) {
						List<AObject> list = ((ServerCommSendListObject) comm).getListObject();
						for(AObject object : list) {
							System.out.println("koordinat yg diterima : " + object.getX() + " " + object.getY());
						}
						Window.getInstance().getGameScene().setListOfObject(((ServerCommSendListObject) comm).getListObject());
					}
				}
				closeSocket();
			}
			else {
				JOptionPane.showMessageDialog(Window.getInstance().getContentPane(), Message.CONNECT_FAIL);
			}
		} catch (EOFException e) {
			// Go to Finally
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				closeSocket();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
