package client.config;

public final class Message {

	public static final String CONNECT_FAIL = "Cannot connect to Server !";
	public static final String USERNAME_EMPTY = "Username is empty !";
	public static final String NAME_ALREADY_USED = "Name already used !";
	public static final String ROOM_IS_FULL = "Room is Full !";
	public static final Object DISCONNECT_FAIL = "Fail disconnect !";
	
}
