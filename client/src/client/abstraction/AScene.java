package client.abstraction;

import javax.swing.JPanel;

import client.config.Config;

public abstract class AScene extends JPanel {

	public AScene() {
		setLayout(null);
		setSize(common.config.Config.WINDOW_HEIGHT+500, common.config.Config.WINDOW_WIDTH+500);
	}
	
	public void requestFocus() {
		setFocusable(true);
		requestFocusInWindow();
	}
	
}
