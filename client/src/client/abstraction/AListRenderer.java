package client.abstraction;

import javax.swing.JLabel;
import javax.swing.ListCellRenderer;

public abstract class AListRenderer<E> extends JLabel implements ListCellRenderer<E> {

	public AListRenderer() {
		setOpaque(true);
		setIconTextGap(12);
	}
	
}
