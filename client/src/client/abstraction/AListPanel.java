package client.abstraction;

import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JScrollPane;

public abstract class AListPanel<E> extends JScrollPane {

	private DefaultListModel<E> listModel;
	private JList<E> list;

	public AListPanel(AListRenderer<E> cellRenderer) {
		setListModel(new DefaultListModel<E>());
		setList(new JList<E>(getListModel()));
		getList().setCellRenderer(cellRenderer);
		getViewport().add(getList());
	}

	private JList<E> getList() {
		return list;
	}

	private void setList(JList<E> list) {
		this.list = list;
	}
	
	private DefaultListModel<E> getListModel() {
		return listModel;
	}

	private void setListModel(DefaultListModel<E> listModel) {
		this.listModel = listModel;
	}
	
	public void addToList(E element) {
		getListModel().addElement(element);
	}
	
	public void clearList() {
		getListModel().clear();
	}
	
	public void refreshList(List<E> listElement) {
		this.clearList();
		for(E element : listElement) {
			getListModel().addElement(element);
		}
	}
	
	public int getListSize() {
		return getListModel().size();
	}
	
	public E getSelectedRow() {
		return getList().getSelectedValue();
	}
	
}
