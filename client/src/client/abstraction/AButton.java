package client.abstraction;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public abstract class AButton extends JButton implements ActionListener {
	
	private IActionEvent action;
	
	public AButton() {
		addActionListener(this);
	}
	
	private IActionEvent getActionEvent() {
		return action;
	}

	public void setActionEvent(IActionEvent action) {
		this.action = action;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		getActionEvent().doActionEvent();
	}
	
}
