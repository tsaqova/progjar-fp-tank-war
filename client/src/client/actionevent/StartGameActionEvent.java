package client.actionevent;

import java.io.IOException;

import client.Client;
import client.abstraction.IActionEvent;

public class StartGameActionEvent implements IActionEvent {

	@Override
	public void doActionEvent() {
		Client client = Client.getInstance();
		try {
			client.getActiveThread().startGame();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
