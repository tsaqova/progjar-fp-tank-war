package client.actionevent;

import java.io.IOException;

import client.Client;
import client.abstraction.IActionEvent;

public class ToTeamBActionEvent implements IActionEvent {

	@Override
	public void doActionEvent() {
		Client client = Client.getInstance();
		try {
			if(!client.getTeamName().equals("Team B")) {
				client.getActiveThread().changeTeam("Team B");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
