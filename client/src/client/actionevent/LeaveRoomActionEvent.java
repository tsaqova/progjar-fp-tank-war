package client.actionevent;

import java.io.IOException;

import client.Client;
import client.abstraction.IActionEvent;

public class LeaveRoomActionEvent implements IActionEvent {

	@Override
	public void doActionEvent() {
		Client client = Client.getInstance();
		try {
			client.getActiveThread().leaveRoom();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
