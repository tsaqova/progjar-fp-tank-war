package client.actionevent;

import java.io.IOException;

import client.Client;
import client.abstraction.IActionEvent;

public class ToTeamAActionEvent implements IActionEvent {

	@Override
	public void doActionEvent() {
		Client client = Client.getInstance();
		try {
			if(!client.getTeamName().equals("Team A")) {
				client.getActiveThread().changeTeam("Team A");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
