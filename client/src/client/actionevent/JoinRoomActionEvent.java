package client.actionevent;

import java.io.IOException;

import javax.swing.JOptionPane;

import com.sun.java.swing.plaf.windows.resources.windows;

import common.clientcomm.ClientCommJoinRoom;
import common.config.CommMessage;
import common.servercomm.ServerCommJoinRoom;
import client.Client;
import client.abstraction.IActionEvent;
import client.component.Window;
import client.config.Message;
import client.scene.Lobby;
import client.thread.ActiveThread;

public class JoinRoomActionEvent implements IActionEvent {

	@Override
	public void doActionEvent() {
		Lobby lobby = Window.getInstance().getLobby();
		if(lobby.getListRoomPanel().getListSize() > 0) {
			String roomName = lobby.getListRoomPanel().getSelectedRow().getName();
			Client.getInstance().getActiveThread().joinRoom(roomName);
		}
	}

}
