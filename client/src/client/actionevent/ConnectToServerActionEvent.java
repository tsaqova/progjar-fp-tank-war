package client.actionevent;

import client.Client;
import client.abstraction.IActionEvent;

public class ConnectToServerActionEvent implements IActionEvent {

	@Override
	public void doActionEvent() {
		Client client = Client.getInstance();
		client.getActiveThread().connect();
	}

}
