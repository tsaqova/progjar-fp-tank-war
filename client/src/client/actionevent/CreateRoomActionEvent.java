package client.actionevent;

import java.io.IOException;

import javax.swing.JOptionPane;

import client.Client;
import client.abstraction.IActionEvent;
import client.component.Window;

public class CreateRoomActionEvent implements IActionEvent {

	@Override
	public void doActionEvent() {
		String roomName = (String)JOptionPane.showInputDialog(
		                    Window.getInstance(),
		                    "Room name :\n",
		                    "Create Room",
		                    JOptionPane.PLAIN_MESSAGE,
		                    null, null,
		                    null);
		
		try {
			if(roomName != null) Client.getInstance().getActiveThread().createRoom(roomName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
