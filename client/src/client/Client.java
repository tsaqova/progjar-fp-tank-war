package client;

import java.io.IOException;
import java.net.UnknownHostException;

import common.component.Player;
import common.component.Status;
import client.thread.PassiveThread;
import client.thread.ActiveThread;

public class Client {

	private static Client instance = new Client();
	private PassiveThread passiveThread;
	private ActiveThread activeThread;
	private String roomName;
	private String userName;
	private String teamName;
	private Status status;
	private int direction;
	
	public static Client getInstance() {
		return instance;
	}
	
	private Client() {
		try {
			setActiveThread(new ActiveThread(this));
			setPassiveThread(new PassiveThread(this));
			setStatus(Status.DISCONNECT);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public PassiveThread getPassiveThread() {
		return passiveThread;
	}

	public void setPassiveThread(PassiveThread passiveThread) {
		this.passiveThread = passiveThread;
	}

	public ActiveThread getActiveThread() {
		return activeThread;
	}

	public void setActiveThread(ActiveThread activeThread) {
		this.activeThread = activeThread;
	}
	
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}
	
}